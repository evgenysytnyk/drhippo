package com.geekhub.j4w.drhippo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrHippoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrHippoApplication.class, args);
	}
}
